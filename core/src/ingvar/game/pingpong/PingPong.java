package ingvar.game.pingpong;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import ingvar.game.pingpong.screens.MenuScreen;

public class PingPong extends Game {

    private Skin skin;

    public Skin getSkin() {
        return skin;
    }
	
	@Override
	public void create () {
        skin = new Skin(Gdx.files.internal("skin-default/uiskin.json"), new TextureAtlas("skin-default/uiskin.atlas"));

        setScreen(new MenuScreen(this));
	}

    @Override
	public void render () {
        super.render();
	}

    @Override
    public void dispose() {
        super.dispose();
    }



}

package ingvar.game.pingpong.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import ingvar.game.pingpong.PingPong;

public class HelpScreen implements Screen {

    private static final String CONTROLS = "W/S - player 1 moves\nUP/DOWN - player 2 moves\nP - pause\nR - reset game";

    private final PingPong game;
    private final Stage stage;

    private Label lblHeader;
    private Label lblControls;
    private Button btnBack;

    public HelpScreen(PingPong g) {
        this.game = g;
        stage = new Stage(new ScreenViewport());
        Gdx.input.setInputProcessor(stage);

        Skin skin = game.getSkin();
        lblHeader = new Label("Controls", skin);
        lblControls = new Label(CONTROLS, skin);
        btnBack = new TextButton("Back", skin);
        btnBack.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new MenuScreen(game));
            }
        });

        Table table = new Table(skin);
        stage.addActor(table);
        table.setFillParent(true);

        table.add(lblHeader);
        table.row();
        table.add(lblControls);
        table.row();
        table.add(btnBack);
    }

    @Override
    public void render(float delta) {
        GL20 gl = Gdx.gl;

        gl.glClearColor(0, 0, 0, 1);
        gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }

}

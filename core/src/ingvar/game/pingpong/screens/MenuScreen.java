package ingvar.game.pingpong.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import ingvar.game.pingpong.PingPong;

public class MenuScreen implements Screen {

    private final PingPong game;
    private Stage stage;

    private final OrthographicCamera camera;
    private final SpriteBatch batch;

    public MenuScreen(PingPong g) {
        this.game = g;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        batch = new SpriteBatch();
        stage = new Stage(new ScreenViewport(camera), batch);
        Gdx.input.setInputProcessor(stage);

        Skin skin = game.getSkin();
        Label header = new Label("Ping Pong", skin);
        TextButton btnPlay = new TextButton("Play game", skin);
        btnPlay.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new GameScreen(game));
            }
        });
        TextButton btnHelp = new TextButton("Help", skin);
        btnHelp.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new HelpScreen(game));
            }
        });

        Table table = new Table(skin);
        table.setFillParent(true);
        stage.addActor(table);

        table.add(header).padBottom(16).top();
        table.row();
        table.add(btnPlay).width(100).padBottom(16);
        table.row();
        table.add(btnHelp).width(100);
    }

    @Override
    public void render(float delta) {
        GL20 gl = Gdx.gl;

        gl.glClearColor(0, 0, 0, 1);
        gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
        Table.drawDebug(stage);
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

}

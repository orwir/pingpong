package ingvar.game.pingpong.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import ingvar.game.pingpong.PingPong;

public class GameScreen implements Screen {

    private static final float FIELD_WIDTH = 120;
    private static final float FIELD_HEIGHT = 80;
    private static final float PLAYER_VELOCITY = FIELD_HEIGHT / 1.6f;
    private static final float BALL_BASE_VELOCITY = FIELD_WIDTH * .5f;
    private static final int PLAYER_1 = 1;
    private static final int PLAYER_2 = 2;

    private final PingPong game;
    private final OrthographicCamera camera;

    private Box2DDebugRenderer debugRenderer;
    private World world;
    private Fixture ball;
    private Fixture player1;
    private Fixture player2;
    private Fixture leftWall;
    private Fixture rightWall;
    private int player1Score;
    private int player2Score;
    private boolean gameRunning;
    private boolean gameResumed;

    private final Stage stage;
    private Label lblScore1;
    private Label lblScore2;
    private Label lblSeparator;
    private Label lblPause;
    private TextButton btnReset;

    public GameScreen(PingPong g) {
        this.game = g;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, FIELD_WIDTH, FIELD_HEIGHT);

        world = new World(new Vector2(0, 0), true);
        world.setContactListener(new GoalListener());
        debugRenderer = new Box2DDebugRenderer();

        stage = new Stage(new ScreenViewport());
        Gdx.input.setInputProcessor(stage);
        Skin skin = game.getSkin();

        lblSeparator = new Label("|", skin);
        lblScore1 = new Label("0", skin);
        lblScore2 = new Label("0", skin);
        lblPause = new Label("Game paused", skin);
        btnReset = new TextButton("Restart", skin);
        btnReset.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                endGame();
            }
        });

        Table table = new Table(skin);
        stage.addActor(table);
        table.setFillParent(true);
        table.top();

        table.add(lblScore1).padRight(16);
        table.add(lblSeparator);
        table.add(lblScore2).padLeft(16);
        table.row().colspan(3);
        table.add(lblPause).padTop(48);
        table.row().colspan(3);
        table.add(btnReset).padTop(16);

        createWalls();
        createPlayer1();
        createPlayer2();
        createBall();
        resumeGame(true);
    }

    @Override
    public void render(float delta) {
        GL20 gl = Gdx.gl;

        gl.glClearColor(0, 0, 0, 1);
        gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();

        if(Gdx.input.isKeyPressed(Input.Keys.R)) {
            endGame();
        }
        if (Gdx.input.isKeyPressed(Input.Keys.SPACE) && !gameRunning) {
            gameRunning = true;
            gameResumed = true;
            player1Score = 0;
            player2Score = 0;
            ball.getBody().setLinearVelocity(BALL_BASE_VELOCITY * (Math.random() > .5 ? 1 : -1), 0);
        }
        if(gameRunning) {
            if (Gdx.input.isKeyPressed(Input.Keys.P)) {
                resumeGame(!gameResumed);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
                player2.getBody().setLinearVelocity(0, PLAYER_VELOCITY);
            } else if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
                player2.getBody().setLinearVelocity(0, -PLAYER_VELOCITY);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.W)) {
                player1.getBody().setLinearVelocity(0, PLAYER_VELOCITY);
            } else if (Gdx.input.isKeyPressed(Input.Keys.S)) {
                player1.getBody().setLinearVelocity(0, -PLAYER_VELOCITY);
            }
        }

        world.step(1/60f, 6, 2);
        player1.getBody().setLinearVelocity(0, 0);
        player2.getBody().setLinearVelocity(0, 0);

        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
        debugRenderer.render(world, camera.combined);
        Table.drawDebug(stage);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        world.dispose();
    }

    private void goal(int player) {
        if(PLAYER_1 == player) {
            player1Score++;
            lblScore1.setText(Integer.toString(player1Score));
        } else {
            player2Score++;
            lblScore2.setText(Integer.toString(player2Score));
        }
    }

    private void endGame() {
        final float playerHeight = Math.max(0.1f, FIELD_HEIGHT * .3f);

        gameRunning = false;
        resumeGame(true);
        ball.getBody().setLinearVelocity(0, 0);
        ball.getBody().setTransform(camera.viewportWidth / 2, camera.viewportHeight / 2, ball.getBody().getAngle());
        player1.getBody().setTransform(Math.max(0.1f, FIELD_WIDTH * .03f), camera.viewportHeight / 2 - playerHeight / 2, player1.getBody().getAngle());
        player2.getBody().setTransform(Math.max(0.1f, FIELD_WIDTH * .95f), camera.viewportHeight / 2 - playerHeight / 2, player2.getBody().getAngle());
    }

    private void resumeGame(boolean isResume) {
        gameResumed = isResume;
        ball.getBody().setActive(isResume);
        player1.getBody().setActive(isResume);
        player2.getBody().setActive(isResume);

        lblPause.setVisible(!isResume);
        btnReset.setVisible(!isResume);
    }

    private void createWalls() {
        final float wall_size = 0.1f;

        BodyDef bottomDef = new BodyDef();
        bottomDef.position.set(camera.viewportWidth / 2, 0);
        Body bottomBody = world.createBody(bottomDef);
        PolygonShape bottomShape = new PolygonShape();
        bottomShape.setAsBox(camera.viewportWidth / 2, wall_size);
        bottomBody.createFixture(bottomShape, 0f);
        bottomShape.dispose();

        BodyDef topDef = new BodyDef();
        topDef.position.set(camera.viewportWidth / 2, camera.viewportHeight);
        Body topBody = world.createBody(topDef);
        PolygonShape topShape = new PolygonShape();
        topShape.setAsBox(camera.viewportWidth / 2, wall_size);
        topBody.createFixture(topShape, 0f);
        topShape.dispose();

        BodyDef rightDef = new BodyDef();
        rightDef.position.set(camera.viewportWidth, camera.viewportHeight / 2);
        Body rightBody = world.createBody(rightDef);
        PolygonShape rightShape = new PolygonShape();
        rightShape.setAsBox(wall_size, camera.viewportHeight / 2);
        rightWall = rightBody.createFixture(rightShape, 0f);
        rightShape.dispose();

        BodyDef leftDef = new BodyDef();
        leftDef.position.set(0, camera.viewportHeight / 2);
        Body leftBody = world.createBody(leftDef);
        PolygonShape leftShape = new PolygonShape();
        leftShape.setAsBox(wall_size, camera.viewportHeight / 2);
        leftWall = leftBody.createFixture(leftShape, 0f);
        leftShape.dispose();
    }

    private void createBall() {
        BodyDef def = new BodyDef();
        def.type = BodyDef.BodyType.DynamicBody;
        def.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2);
        def.allowSleep = true;

        Body body = world.createBody(def);
        body.setBullet(true);

        CircleShape shape = new CircleShape();
        shape.setRadius(Math.max(0.1f, FIELD_WIDTH * .01f));

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 0.1f;
        fixtureDef.friction = 0f;
        fixtureDef.restitution = 1f;

        ball = body.createFixture(fixtureDef);
        shape.dispose();
    }

    private void createPlayer1() {
        final float playerWidth = Math.max(0.1f, FIELD_WIDTH * .02f);
        final float playerHeight = Math.max(0.1f, FIELD_HEIGHT * .3f);

        BodyDef def = new BodyDef();
        def.type = BodyDef.BodyType.DynamicBody;
        def.position.set(Math.max(0.1f, FIELD_WIDTH * .03f), camera.viewportHeight / 2 - playerHeight / 2);
        Body body = world.createBody(def);

        PolygonShape shape = new PolygonShape();
        shape.set(new float[] {0,0, 0,playerHeight,
                /*front line*/ playerWidth*1.4f,playerHeight*.5f,
                playerWidth,playerHeight, playerWidth,0});

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 10000f;
        fixtureDef.friction = 0f;
        fixtureDef.restitution = 0f;

        player1 = body.createFixture(fixtureDef);
        shape.dispose();
    }

    private void createPlayer2() {
        final float playerWidth = Math.max(0.1f, FIELD_WIDTH * .02f);
        final float playerHeight = Math.max(0.1f, FIELD_HEIGHT * .3f);

        BodyDef def = new BodyDef();
        def.type = BodyDef.BodyType.DynamicBody;
        def.position.set(Math.max(0.1f, FIELD_WIDTH * .95f), camera.viewportHeight / 2 - playerHeight / 2);
        Body body = world.createBody(def);

        PolygonShape shape = new PolygonShape();
        shape.set(new float[] {0,0, 0,playerHeight,
                /*front line*/ -(playerWidth*.4f),playerHeight*.5f,
                playerWidth,playerHeight, playerWidth,0});

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 10000f;
        fixtureDef.friction = 0f;
        fixtureDef.restitution = 0f;

        player2 = body.createFixture(fixtureDef);
        shape.dispose();
    }

    private class GoalListener implements ContactListener {

        @Override
        public void beginContact(Contact contact) {
            if(leftWall.equals(contact.getFixtureA()) || leftWall.equals(contact.getFixtureB())) {
                goal(PLAYER_2);
            }
            if(rightWall.equals(contact.getFixtureA()) || rightWall.equals(contact.getFixtureB())) {
                goal(PLAYER_1);
            }
        }

        @Override
        public void endContact(Contact contact) {

        }

        @Override
        public void preSolve(Contact contact, Manifold oldManifold) {

        }

        @Override
        public void postSolve(Contact contact, ContactImpulse impulse) {

        }

    }

}
